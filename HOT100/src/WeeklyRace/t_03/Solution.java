package WeeklyRace.t_03;

import java.util.*;

class Solution {


    private static long sum = 0;


    public static long maxSum(List<Integer> nums, int m, int k) {

        if (nums.size() == 1 && m == 1 && k == 1) {
            return nums.get(0);
        }

        long result = 0;
        Integer[] integers = new Integer[nums.size()];
        for (int i = 0; i < nums.size(); i++) {
            integers[i] = nums.get(i);
        }
        for (int i = 0; i + k <= nums.size(); i++) {
            int temp = -1;
            int end = nums.get(i + k - 1);
            if (i > 0) {
                temp = nums.get(i - 1);
            }
            Integer[] subArray = Arrays.copyOfRange(integers, i, i + k);
            result = Math.max(result, maxSumSubarrayWithMUnique(subArray, m, temp, end));
        }

        return result;
    }


    public static boolean hasAtLeastMDistinctElements(Integer[] array, int m) {
        Set<Integer> distinctElements = new HashSet<>();
        for (int num : array) {
            distinctElements.add(num);
            if (distinctElements.size() >= m) {
                return true;
            }
        }

        return false;
    }

    private static long maxSumSubarrayWithMUnique(Integer[] nums, int m, int temp, int end) {
        boolean b = hasAtLeastMDistinctElements(nums, m);
        if (!b) {
            return 0;
        }
        if (sum == 0) {
            for (Integer num : nums) {
                sum += num;
            }
        }
        if (temp != -1) {
            sum -= temp;
            sum += end;
        }
        return sum;
    }

    public static void main(String[] args) {
        Integer[] numbers = {1, 1, 1, 3};
        List<Integer> integers = Arrays.asList(numbers);
        int m = 2, k = 2;
        long i = maxSum(integers, m, k);
        System.out.println(i);
    }
}
