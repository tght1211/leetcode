package WeeklyRace.t_02;

import java.util.Arrays;

public class Solution {
    public static boolean isScramble(String s1, String s2) {
        String result1 = getResult(s1);
        String result2 = getResult(s2);
        return result1.equals(result2);
    }



    public static String getResult(String s1){
        // 创建奇数位置字符数组
        char[] oddChars = new char[s1.length()];
        // 创建偶数位置字符数组
        char[] evenChars = new char[s1.length()];

        // 提取奇数位置字符和偶数位置字符
        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);
            if (i % 2 == 0) {
                evenChars[i / 2] = c;
            } else {
                oddChars[i / 2] = c;
            }
        }

        // 对两个数组进行排序
        Arrays.sort(oddChars);
        Arrays.sort(evenChars);

        String s = new String(oddChars);
        String s2 = new String(evenChars);
        return s+":"+s2;
    }

    public static void main(String[] args) {
        String s1 = "abe", s2 = "bea";
        boolean scramble = isScramble(s1, s2);
        System.out.println(scramble);
    }
}

