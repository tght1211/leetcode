package WeeklyRace.t_01;

import java.util.Arrays;

public class Solution2 {
    public static boolean areAlmostEqual(String s1, String s2) {

        char[][] c1 = new char[2][26], c2 = new char[2][26];
        for (int i = 0; i < s1.length(); i++) {
            c1[i % 2][s1.charAt(i) - 'a']++;
            c2[i % 2][s2.charAt(i) - 'a']++;
        }
        return Arrays.deepEquals(c1, c2);

    }

    public static void main(String[] args) {
        String s1 = "abcd";
        String s2 = "cdab";
        boolean result = areAlmostEqual(s1, s2);
        System.out.println(result); // 应该输出 true
    }
}
