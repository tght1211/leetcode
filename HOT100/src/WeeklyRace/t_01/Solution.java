package WeeklyRace.t_01;

class Solution {
    public static boolean canBeEqual(String s1, String s2) {
        if(s1.equals(s2)){
            return true;
        }

        char[] chars = s1.toCharArray();
        String res = new String(s1);
        char[] temp = res.toCharArray();
        for (int i = 0; i+2 < chars.length; i++) {
            char tc =  temp[i];
            temp[i] = temp[i+2];
            temp[i+2] = tc;
            String s = new String(temp);
            if (s.equals(s2)){
                return true;
            }
        }


        for (int i = 0; i+2 < chars.length; i++) {
            char[] temp1 = res.toCharArray();
            char tc =  temp1[i];
            temp1[i] = temp1[i+2];
            temp1[i+2] = tc;
            String s = new String(temp1);
            if (s.equals(s2)){
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        // s1 = "abcdba", s2 = "cabdab"
        canBeEqual("abcdba","cabdab");
    }
}
