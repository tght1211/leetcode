package H_01simple.h_2_t_20_有效的括号;


/**
 * @author Tght
 * '('，')'，'{'，'}'，'['，']'
 */
public class Solution {
    public boolean isValid(String s) {
        int length = s.length() / 2;
        for (int i = 0; i < length; i++) {
            // (--{})  ,--区域就是被替换的
            s = s.replace("()", "").replace("{}", "").replace("[]", "");
        }
        return s.length() == 0;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        // boolean valid = solution.isValid("((){})");
        // System.out.println(valid);
        String s = "((){})";
        String replace = s.replace("()", "");

        System.out.println(replace);
    }
}
