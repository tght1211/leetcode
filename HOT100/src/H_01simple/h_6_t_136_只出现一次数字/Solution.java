package H_01simple.h_6_t_136_只出现一次数字;

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public int singleNumber(int[] nums) {
        // Set集合，无重复元素，有序。添加不成功就删除。
        Set<Integer> cache = new HashSet<>();
        cache.add(1);
        cache.add(2);
        cache.add(3);

/*        for (int i = 0; i < nums.length; i++) {
            if (!cache.add((nums[i]))) {
                cache.remove(nums[i]);
            }
        }*/

/*        for (Integer integer : cache) {
            if (integer == 2){
                cache.remove(integer);
            }
        }*/
        // next：指针下移，返回该指针所指向的元素
        return cache.iterator().next();
    }

    public static void main(String[] args) {
        int [] arr = {1,1,3,3,4,4,5,6,6};
        Solution solution = new Solution();
        int i = solution.singleNumber(arr);
        System.out.println(i);
    }
}
