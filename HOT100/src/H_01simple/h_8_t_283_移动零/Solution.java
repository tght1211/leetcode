package H_01simple.h_8_t_283_移动零;

public class Solution {
    public void moveZeroes(int[] nums) {
        // 用来进位操作每个元素
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0){
                // 把不为0的丢到前面去，逆向思维，不是丢0到后面
                nums[j++] = nums[i];
                // 接着自己+1，安排下一个不为0的元素入驻
            }
        }
        while(j < nums.length){
            // 前面不为0的元素入驻完，j++在上面已经指向下一地址，开始对0的投放
            nums[j++] = 0;
        }
    }

    public static void main(String[] args) {
        int[] arr = {0, 0};
        Solution solution = new Solution();
        solution.moveZeroes(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
