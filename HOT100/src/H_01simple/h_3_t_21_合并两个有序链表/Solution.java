package H_01simple.h_3_t_21_合并两个有序链表;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Tght
 * 什么是递归，递归就是调用自己，根据一个条件判断来停止调用
 * 124 134
 *  124 134 134 12344 112344 34 124 124 2344 12344 24 34 24 344 2344 4 34 34 44 344 4 4 4 4 44 null 4 4
 *
 *  链表就是不能随机查找的，只有头。
 */
public class Solution {

    public ListNode reverseList(ListNode head) {
        // 遍历链表，进行存入到数组，再反转，再存入链表
        ListNode dummy = new ListNode(0);
        List list = new ArrayList();
        while(head != null){
            list.add(head.val);
            head = head.next;
        }
        Collections.reverse(list);
        for(Object list1 : list){
            dummy.next = new ListNode((int)list1);
            dummy = dummy.next;
        }
        return dummy.next;
    }

}

/*
          if (list1 == null) {
              return list2;
          }
          if (list2 == null) {
              return list1;
          }
          ListNode res = list1.val < list2.val ? list1 : list2;
          res.next = mergeTwoLists(res.next, list1.val >= list2.val ? list1 : list2);
          return res;

*/
