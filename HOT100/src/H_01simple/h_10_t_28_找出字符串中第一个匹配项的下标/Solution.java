package H_01simple.h_10_t_28_找出字符串中第一个匹配项的下标;

class Solution {
    public static int strStr(String haystack, String needle) {
        if(needle.length() > haystack.length()){
            return -1;
        }

        int p1 = 0;
        int p2 = 0;
        int p = 0;

        for(int i=0; i < haystack.length() && p < needle.length(); i++){
            if(haystack.charAt(i) == needle.charAt(p)){
                if(p==0){
                    p1 = i;
                }
                p2 = i;
                p++;
            }else{
                i = i - p;
                p1 = 0;
                p2 = 0;
                p = 0;
            }
        }
        if(p != needle.length()){
            return -1;
        }
        return p1;
    }

    public static void main(String[] args) {
        strStr("mississippi","issipi");
    }
}