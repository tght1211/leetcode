package H_02medium.h_2_t_209_长度最小的子数组;

class Solution {
    public static int minSubArrayLen(int target, int[] nums) {
        int left = 0;
        int min = nums.length;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (sum >= target){
                min = Math.min(min, i-left+1);
                sum = 0;
                i = left;
                left++;
            }
        }
        return left == 0 ? 0:min;
    }

    public static void main(String[] args) {
        int[] arr = {2,3,1,2,4,3};
        minSubArrayLen(7,arr);
    }
}