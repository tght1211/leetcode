package H_02medium.h_1_t_6_N字形变化;

class Solution {
    public static String convert(String s, int numRows) {
        int roll = numRows*2-2;
        if(roll == 0){
            return s;
        }
        int [] arr = new int [s.length()];
        for(int i = 0;i < s.length(); i++){
            int mo = i%roll;
            if(mo >= numRows-1){
                // 第二波
                arr[i] = roll - mo + 1;
            }else{
                arr[i] = mo+1;
            }
        }
        StringBuilder builder = new StringBuilder();
        for(int j=1; j <= numRows; j++){
            for(int i=0; i < arr.length; i++){
                if(arr[i] == j){
                    builder.append(s.charAt(i));
                }
            }
        }
        return builder.toString();

    }

    public static void main(String[] args) {
        convert("ABCD",5);
    }
}
// PAYP ALIS HIRI NG