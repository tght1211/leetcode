package H_02medium.h_3_t_3_无重复字符的最长子串;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static int lengthOfLongestSubstring(String s) {
        List<String> list = new ArrayList();
        int left = 0;
        int maxLen = 0;
        int resLen = 0;
        for(int i = 0; i < s.length(); i++){
            String res = String.valueOf(s.charAt(i));
            while (list.contains(res)){
                list.remove(String.valueOf(s.charAt(left)));
                left++;
                maxLen--;
            }
            list.add(res);
            maxLen++;
            resLen = Math.max(resLen,maxLen);
        }
        return resLen;
    }

    public static void main(String[] args) {
        lengthOfLongestSubstring("pwwkew");
    }
}